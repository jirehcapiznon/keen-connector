FROM node:boron

MAINTAINER Reekoh

COPY . /home/node/keen-connector

WORKDIR /home/node/keen-connector

RUN npm i --production

CMD ["node", "app.js"]
