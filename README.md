## Keen Connector

## Description

The Keen Connector Plugin for the Reekoh IoT Platform. This plugin enables you to send and synchronize device data to the Keen platform for analytics and data visualization

## Configuration

Keen Connector Plugin configuration can be done once you've created your own pipeline in Reekoh. To configure the plugin, you will be asked to provide the following details:
 
- **Connector Name** - This is a label given to your plugin to locate it easily in your pipeline.
- **Project ID** -  This is your Keen project ID.
- **Write Key** - This is the write key for your Keen project.
- **Collection** - This is the collection where data will be sent to.

![](https://reekohdocs.blob.core.windows.net/plugin-docs/connector/keen-io-connector/1.0.0/keen-io-config.png)

## Send Data

In order to simulate sending data, You will need a Gateway Plugin to send the data to Keen Connector Plugin. In the screenshot below, it uses HTTP Gateway Plugin. Note: Look for the documentation on how to use this plugin.

![](https://reekohdocs.blob.core.windows.net/plugin-docs/connector/keen-io-connector/1.0.0/keen-io-pipeline.png)

Make sure your plugins and pipeline are successfully deployed.

Using **POST MAN** as HTTP Client simulator, you can now simulate sending data. 

![](https://reekohdocs.blob.core.windows.net/plugin-docs/connector/keen-io-connector/1.0.0/keen-io-postman.png)

## Data format

The HTTP Gateway only accepts data in JSON format. Also, a "device" field is required to be in the request body. This device field should contain a device ID which is registered in Reekoh's Devices Management.

**Sample Input Data**
```javascript
{
  "device": "203cd69f-238c-5fa1-b757-684bec35a6f1",
  "key1": "value1", 
  "Key2": 121, 
  "key3": 40
}
```

## Verify Data

The data will be ingested by the HTTP Gateway plugin, which will be forwarded to all the other plugins that are connected to it in the pipeline.

To verify if the data is ingested properly in the pipeline, you need to check the **LOGS** tab in every plugin in the pipeline. All the data that have passed through the plugin will be logged in the **LOGS** tab.
If an error occurs, an error exception will be logged in the **EXCEPTIONS** tab.

![](https://reekohdocs.blob.core.windows.net/plugin-docs/connector/keen-io-connector/1.0.0/keen-io-logs.png)

The data that was sent to HTTP Gateway will be forwarded to Keen Connector. Log in your Keen account and look for the **Collection** name you set in the configuration. The data that has been sent should appear in there.

![](https://reekohdocs.blob.core.windows.net/plugin-docs/connector/keen-io-connector/1.0.0/keen-io-received.png)
